from django.contrib import admin
from .models import Choice, Station

admin.site.register(Choice)
# Register your models here.
class ChoiceInline(admin.TabularInline):
    model = Choice
    fk_name = 'station'
    extra = 1

class StationAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline, ]
    list_display = ['name', 'text', "get_choices"]
    def get_choices(self, obj):
        return " / ".join([str(o) for o in obj.choice_set.all()])
    get_choices.short_description = "choices"


admin.site.register(Station, StationAdmin)
