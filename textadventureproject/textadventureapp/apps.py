from django.apps import AppConfig


class TextadventureappConfig(AppConfig):
    name = 'textadventureapp'
