from django.db import models

# Create your models here.
class Station(models.Model):
    name = models.CharField(max_length=50, unique=True)
    title = models.CharField(max_length=200)
    text = models.TextField()

    def __str__(self):
        return self.name

class Choice(models.Model):
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    next_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="incoming_set")

    def __str__(self):
        return f"{self.text}: {self.next_station.name}"

