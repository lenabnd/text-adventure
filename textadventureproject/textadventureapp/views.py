from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse

from .models import Choice, Station

# Create your views here.

def station(request, name="start"):
    station = get_object_or_404(Station, name=name) 
    return render(request, "textadventureapp/station.html", {"station": station})

